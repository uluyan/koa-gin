# koa-gin

A gin-like log middleware for koa

![console](./console.png)

## Quick start

### Install

```
yarn add koa-gin 
```

or 

```
npm install --save koa-gin
```

### Usage

```js
'use strict';

const Koa = require('koa');
const gin = require('koa-gin');

const app = new Koa();
app.use(gin()); // or app.use(gin(console)); for example
app.listen(8000);

```
