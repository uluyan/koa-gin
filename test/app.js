'use strict';

const Koa = require('koa');
const Router = require('koa-router');
const gin = require('..');

const app = new Koa();
const router = new Router();

router
    .get('/ping', (ctx) => ctx.body = 'pong')
    .delete('/ping', (ctx) => ctx.body = 'pong')
    .put('/ping', (ctx) => ctx.body = 'pong')
    .post('/ping', (ctx) => ctx.body = 'pong')
    .get('/boom', () => {
        throw new Error('Boom!');
    })
    .get('/jump', (ctx) => ctx.redirect('/ping'));

app
    .use(gin())
    .use(router.routes())
    .use(router.allowedMethods())
    .listen(11011);
