/**
 * A gin-like log middleware for koa
 */

require('colors');

module.exports = (log = console) => {
    return async (ctx, next) => {
        const startTime = Date.now();
        const address = getRemoteAddress(ctx);

        let message;
        try {
            await next();
        } catch (err) {
            message = err.message;
        }
        const elapsed = Date.now() - startTime;
        log.info(formatLog(ctx.response.status, elapsed, address,
            ctx.request.method, ctx.request.url, message));
    };
};

// Helpers

function getRemoteAddress(ctx) {
    const remoteAddress = ctx.req.headers['x-forwarded-for'] ||
        ctx.req.connection.remoteAddress ||
        ctx.req.socket.remoteAddress || '';
    return remoteAddress.startsWith('::ffff:') ?
        remoteAddress.replace('::ffff:', '') : remoteAddress;
}

function formatLog(status, elapsed, address, method, url, err) {
    const statusColor = (status >= 200 && status < 300) ? 'bgGreen' :
        (status < 400) ? 'bgYellow' : 'bgRed';
    const methodColor = {
        GET: 'bgBlue',
        POST: 'bgGreen',
        PUT: 'bgYellow',
        DELETE: 'bgRed'
    }[method] || 'bgMagenta';
    const message = err ? ` <${err.bgRed.white}>` : '';
    const pad = (str) => ` ${str} `;

    return [
        '',
        pad(status.toString())[statusColor].white,
        pad(`${elapsed}ms`.padStart(8)),
        pad(address.padStart(15)),
        ` ${pad(method)[methodColor].white.padEnd(30)}${url}${message}`,
    ].join('|');
}
